<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class ScriptHandler {

  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();

    $dirs = [
      'modules',
      'profiles',
      'themes',
    ];

    // Required for unit testing
    foreach ($dirs as $dir) {
      if (!$fs->exists($drupalRoot . '/' . $dir)) {
        $fs->mkdir($drupalRoot . '/' . $dir);
        $fs->touch($drupalRoot . '/' . $dir . '/.gitignore');
      }
    }

    // Create some new folders
    $composerRoot = $drupalFinder->getComposerRoot();
    $configDir = $composerRoot . '/config/default';
    $privateDir = $composerRoot . '/private/default';
    $additionalDirs = [
      $configDir,
      $privateDir,
    ];
    foreach ($additionalDirs as $dir) {
      if (!$fs->exists($dir)) {
        $fs->mkdir($dir);
        $fs->touch($dir . '/.gitignore');
      }
    }

    // Prepare the settings file for installation
    $localSettingsFile = $drupalRoot . '/sites/default/settings.local.php';
    if (!$fs->exists($localSettingsFile) && $fs->exists($drupalRoot . '/sites/default/settings.local.php.dist')) {
      $fs->copy($drupalRoot . '/sites/default/settings.local.php.dist', $localSettingsFile);
      $event->getIO()->write('Create a sites/default/settings.local.php file that uses environment variables');
      $fs->chmod($localSettingsFile, 0666);
      $event->getIO()->write('chmod 0666 the local settings file');
    }

    $settingsFile = $drupalRoot . '/sites/default/settings.php';
    if (!$fs->exists($settingsFile) && $fs->exists($drupalRoot . '/sites/default/default.settings.php')) {
      $fs->copy($drupalRoot . '/sites/default/default.settings.php', $settingsFile);
      require_once $drupalRoot . '/core/includes/bootstrap.inc';
      require_once $drupalRoot . '/core/includes/install.inc';
      $settings['config_directories'] = [
        CONFIG_SYNC_DIRECTORY => (object) [
          'value' => Path::makeRelative($configDir . '/sync', $drupalRoot),
          'required' => TRUE,
        ],
      ];
      drupal_rewrite_settings($settings, $settingsFile);
      $fs->chmod($settingsFile, 0666);
      $event->getIO()->write('Create a sites/default/settings.php file with chmod 0666');
    }

    // $localEnvFile = $composerRoot . '/.env';
    // if (!$fs->exists($localEnvFile) && $fs->exists($localEnvFile . '.dist')) {
    //   $fs->copy($localEnvFile . '.dist', $localEnvFile);
    //   $event->getIO()->write('Create a .env file from template .env.dist');
    // }

    // $dockerComposeFile = $composerRoot . '/docker-compose.dev.yml';
    // if (!$fs->exists($dockerComposeFile) && $fs->exists($dockerComposeFile . '.dist')) {
    //   $fs->copy($dockerComposeFile . '.dist', $dockerComposeFile);
    //   $event->getIO()->write('Create a docker-compose.dev.yml file from template docker-compose.dev.yml.dist');
    // }

    // Create the files directory with chmod 0777
    if (!$fs->exists($drupalRoot . '/sites/default/files')) {
      $oldmask = umask(0);
      $fs->mkdir($drupalRoot . '/sites/default/files', 0777);
      umask($oldmask);
      $event->getIO()->write('Create a sites/default/files directory with chmod 0777');
    }

    // Make ./bin executable again
      $fs->chmod($composerRoot . '/bin', 0755, 0000, true);
  }

  /**
   * Checks if the installed version of Composer is compatible.
   *
   * Composer 1.0.0 and higher consider a `composer install` without having a
   * lock file present as equal to `composer update`. We do not ship with a lock
   * file to avoid merge conflicts downstream, meaning that if a project is
   * installed with an older version of Composer the scaffolding of Drupal will
   * not be triggered. We check this here instead of in drupal-scaffold to be
   * able to give immediate feedback to the end user, rather than failing the
   * installation after going through the lengthy process of compiling and
   * downloading the Composer dependencies.
   *
   * @see https://github.com/composer/composer/pull/5035
   */
  public static function checkComposerVersion(Event $event) {
    $composer = $event->getComposer();
    $io = $event->getIO();

    $version = $composer::VERSION;

    // The dev-channel of composer uses the git revision as version number,
    // try to the branch alias instead.
    if (preg_match('/^[0-9a-f]{40}$/i', $version)) {
      $version = $composer::BRANCH_ALIAS_VERSION;
    }

    // If Composer is installed through git we have no easy way to determine if
    // it is new enough, just display a warning.
    if ($version === '@package_version@' || $version === '@package_branch_alias_version@') {
      $io->writeError('<warning>You are running a development version of Composer. If you experience problems, please update Composer to the latest stable version.</warning>');
    }
    elseif (Comparator::lessThan($version, '1.0.0')) {
      $io->writeError('<error>Drupal-project requires Composer version 1.0.0 or higher. Please update your Composer before continuing</error>.');
      exit(1);
    }
  }

}
